# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Field,Item



class PlayerDetailItem(Item):
  playerid = Field()
  name = Field()
  birthdate = Field()
  association = Field()
  records = Field()
  match_records = Field()



class PlayerIdItem(Item):
  pid = Field()
