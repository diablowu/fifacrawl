# -*- coding: utf-8 -*-

# Scrapy settings for fifacrawl project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

import os

BOT_NAME = 'fifacrawl'

SPIDER_MODULES = ['fifacrawl.spiders']
NEWSPIDER_MODULE = 'fifacrawl.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36'


ITEM_PIPELINES = {
  'fifacrawl.pipelines.PlayIdSavePipeline': 0,
  'scrapy_mongodb.MongoDBPipeline': 1
}

MONGODB_URI = 'mongodb://10.0.22.151:32768'
MONGODB_DATABASE = os.getenv('fifacrawl_db', 'fifa')
MONGODB_COLLECTION = 'player'
MONGODB_UNIQUE_KEY = 'playerid'


MONGODB_PLAYER_ID_COLLECTION = 'playerid'
