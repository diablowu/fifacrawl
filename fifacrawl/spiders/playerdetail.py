# -*- coding: utf-8 -*-
import scrapy


from pymongo import errors
from pymongo.mongo_client import MongoClient
from pymongo.mongo_replica_set_client import MongoReplicaSetClient
from pymongo.read_preferences import ReadPreference

from fifacrawl.items import PlayerDetailItem
from string import whitespace

from fifacrawl.extractor.player import *

from scrapy.utils.project import get_project_settings


def lowertr(title):
  """
  make all string to lower case and strip all whitespace
  """
  if isinstance(title, unicode):
    return title.encode('utf-8').translate(None, whitespace).lower()
  else:
    return title





class PlayerDetailSpider(scrapy.Spider):
  name = "playerdetail"
  allowed_domains = ["fifa.com"]

  def __init__(self, max=10, pa=10, pn=0, *args, **kwargs):
    super(PlayerDetailSpider, self).__init__(*args, **kwargs)
    urltpl = "http://www.fifa.com/worldfootball/statisticsandrecords/players/player=%s/index.html"
    self.start_urls = ['http://www.baidu.com/']
    settings = get_project_settings()

    # Connecting to a stand alone MongoDB
    connection = MongoClient(settings['MONGODB_URI'])
    # Set up the collection
    database = connection[settings['MONGODB_DATABASE']]
    col = database[settings['MONGODB_PLAYER_ID_COLLECTION']]
    pidlist = col.find(
        filter = {'pid': {'$mod':[int(pa), int(pn)] }} ,
        limit= int(max)
      )
    
    self.start_urls = [ urltpl % pid['pid'] for pid in pidlist ]


  def parse(self, response):
    player = PlayerDetailItem()
    
    player['playerid'] = int(response.url 
      .replace('http://www.fifa.com/worldfootball/statisticsandrecords/players/player=','') 
      .replace('/index.html','') )
    player['match_records'] = dict()

    # base content
    selector = response.xpath('//div[@class="wfStats player"]/div[@class="info"]')

    [ self.extract(player, s) for s in selector]

    return player

  def extract(self, player, s):
    title = lowertr(s.xpath('./table/caption/text()').extract()[0].strip())
    _extractor = self.extractors[title]
    _extractor(player, s)







  extractors = {
    lowertr(u'Record at FIFA Tournaments') : extract_record ,
    lowertr(u'FIFA Confederations Cup') : extract_cfd_cup ,
    lowertr(u'FIFA World Cup™ Qualifiers') : extract_wc_qualifiers ,
    lowertr(u'FIFA World Cup™') : extract_wc ,
    lowertr(u"Men's Olympic Football Tournament") : extract_olympic ,
    lowertr(u"FIFA Club World Cup") : extract_club_cup ,
    lowertr(u"FIFA U-20 World Cup") : extract_u20 ,
    lowertr(u"FIFA U-17 World Cup") : extract_u17
  }
